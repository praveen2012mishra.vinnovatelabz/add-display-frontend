import React, { Component } from "react";
import uuid from "uuid";
import "./shop.css";
import ShopBox from "../Shop_Box/ShopBox";
import axios from "axios";


let selectedItemsArray=[];
class Shop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [],
      selectedId:''
    };
  }
  componentDidMount() {
    const headers = {
      // "Access-Control-Allow-Origin": "*",
      // 'Content-Type': 'application/json',
      // 'Accept': 'application/json'
    };
    //debugger
    axios.get("/showAddProduct", { headers: headers }).then((res) => {
      this.setState({ product: res.data });
      //debugger
    });
  }

  selectedItem=(e)=>{
    selectedItemsArray.push(e.currentTarget.id)
    //console.log(selectedItemsArray, "-------------------->");
    const options = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    //this.setState({selectedItemsArray:newArray})
    axios
      .post("/addCart", selectedItemsArray, options)
      .then((res) => {
        console.log("RESPONSE ==== : ", res);
      })
      .catch((err) => {
        console.log("ERROR: ====", err);
      });
  }

  deleteSelectedItem=(e)=>{
    let { product } = this.state;
    console.log(e.currentTarget.id);
    let id=e.currentTarget.id;
    let newArray=product.filter((ele)=>{
      console.log(ele.id!==id);
      
      return ele.itemId!==id
    })
    this.setState({product:Object.assign([],newArray)})
  }

  render() {
    let { product } = this.state;
    
    return (
      <React.Fragment>
        <div className="shop-box">
          {product.length > 0
            ? product.map(
                (ele) =>ele&& <ShopBox product={ele} click={(e)=>{this.selectedItem(e)}} delete={(e)=>{this.deleteSelectedItem(e)}} firstButtonName={'Add cart'} secondButtonName={'Delete'}/>
              )
            : ""}
        </div>
      </React.Fragment>
    );
  }
}
export default Shop;
