import React, { Component } from "react";
import uuid from "uuid";
import "./display.css";
import { withRouter } from "react-router";
import Shop from "../Shop/shop";
import AddMessage from "../Add_Message/AddMessage"
import DisplayCart from "../DisplayCart/DisplayCart"
import Payment from '../Payment/Payment'
import { Route, Switch } from "react-router-dom";

class Display extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    //this.props.history.push('link');
    let { history } = this.props;
   // console.log(history.location["pathname"]);

    return (
      <React.Fragment>
        <Route exact path="/Payment" component={Payment} />
        <Route exact path="/showAddProductForm" component={Shop} />
        <Route exact path="/Add-Product" component={AddMessage} />
        <Route exact path="/Cart" component={DisplayCart} />
      </React.Fragment>
    );
  }
}
export default withRouter(Display);
