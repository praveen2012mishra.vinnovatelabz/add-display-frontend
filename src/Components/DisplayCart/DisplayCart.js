import React, { Component } from "react";
import uuid from "uuid";
import { withRouter } from "react-router";
import "./display-cart.css";
import axios from "axios";
import ShopBox from '../Shop_Box/ShopBox'

class DisplayCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      getCartArray: [],
      selectedId:''
    };
  }
  componentDidMount() {
    const headers = {
      // "Access-Control-Allow-Origin": "*",
      // 'Content-Type': 'application/json',
      // 'Accept': 'application/json'
    };
    //debugger
    axios.get("/getCart", { headers: headers }).then((res) => {
      console.log(res.data);
      
        this.setState({ getCartArray: res.data });
      //debugger
    });
  }

  selectedItem=(link)=>{
    this.props.history.push(link); 
  }

  deleteSelectedItem=(e)=>{
    let { getCartArray } = this.state;
    console.log(e.currentTarget.id);
    let id=e.currentTarget.id;
    let newArray=getCartArray.filter((ele)=>{
      console.log(ele.id!==id);
      
      return ele.itemId!==id
    })
let data={
  id:e.currentTarget.id
}
const options = {
  headers: {
    "Content-Type": "application/json",
  },
};

    axios
      .post("/deleteItem",  data, options)
      .then((res) => {
        console.log("RESPONSE ==== : ", res);
      })
      .catch((err) => {
        console.log("ERROR: ====", err);
      });
    this.setState({getCartArray:Object.assign([],newArray)})
  }



  render() {
    let { getCartArray } = this.state;
    //console.log(getCartArray,'---------------->');
    
    return (
      <React.Fragment>
        <div className="shop-box">
          {getCartArray.length > 0
            ? getCartArray.map(
                (ele) =>ele&& <ShopBox product={ele} click={()=>{this.selectedItem('/Payment')}} delete={(e)=>{this.deleteSelectedItem(e)}} firstButtonName={'Pay'} secondButtonName={'Delete'}/>
              )
            : ""}
        </div>
      </React.Fragment>
    );
  }
}
export default withRouter(DisplayCart);
