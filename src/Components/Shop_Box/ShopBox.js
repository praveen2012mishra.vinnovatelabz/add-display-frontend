

import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import "./shop_box.css";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin:40,
    backgroundColor:"#ccccd1",
    // display:'flex',
    // flexDirection:'column',
    // justifyContent:'center',
    // alignItems:'center'
  },
  media: {
    height: 600,
    width:300,    
    //backgroundColor:'#ebeaeb !important'
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    borderRadius: 3,
    border: 0,
    color: 'white',
    //height: 48,
    padding: '0 30px',
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
  },
  buttonCart:{
    backgroundColor:'blue'
  }
});

export default function ShopBox(props) {
  const classes = useStyles();
  const getSelectedItem=(e)=>{
    //console.log(e.currentTarget,'================>');
    props.click(e)
  }

  const deleteSelectedItem=(e)=>{
    props.delete(e)
  }
  return (
    <div>{
      props.product.itemName && props.product.itemType&&(
        <Card className={classes.root}>
        <CardActionArea>
          <div className="img-box">
          
          <CardMedia
            className={classes.media} //{classes.media}
            image={`${props.product.itemImage}`}
            title={`${props.product.itemType}`}
          />
          </div>
          
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
            {props.product.itemName}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
            {props.product.itemType}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button size="small" color="green" className={classes.buttonCart}  onClick={getSelectedItem} id={props.product.itemId}>
          {props.firstButtonName}
          </Button>
          <Button size="small" color="red" className={classes.buttonCart} onClick={deleteSelectedItem} id={props.product.itemId}>
          {props.secondButtonName}
          </Button>
        </CardActions>
      </Card>
      )
      }
     
    </div>
    
  );
}

