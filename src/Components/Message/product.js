import React, { Component } from "react";
import uuid from "uuid";
import "./product.css";
import { withRouter } from "react-router";
import Display from '../Display/Display'
import Payment from '../Payment/Payment'

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  changeLinkRouter=(link)=>{
    this.props.history.push(link); 
  }

  render() {
    return (
      <React.Fragment>
        <div className="header-router row ml-0">
          <div className="col-6 col-sm-6 row header-box">            
		  <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-2 pl-4">
              {" "}
              <button onClick={() => this.changeLinkRouter("/showAddProductForm")} className="product-button">Shop</button>
            </div>
            <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-2">
              {" "}
              <button onClick={() => this.changeLinkRouter("/Products")} className="product-button">
                Products
              </button>
            </div>
            <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-2">
              {" "}
              <button onClick={() => this.changeLinkRouter("/Cart")} className="product-button">Cart</button>
            </div>
            <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-2">
              {" "}
              <button onClick={() => this.changeLinkRouter("/Add-Product")} className="product-button">
                Add Product
              </button>
            </div>
            <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-2">
              {" "}
              <button onClick={() => this.changeLinkRouter("/Admin-Products")} className="product-button">
                Admin Products
              </button>
              
            </div>
            {/* <div className="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2 px-2">
              {" "}
              <button onClick={() => this.changeLinkRouter("/Payment")} className="product-button">
              Payment
              </button>
            </div> */}
          </div>
          <div className="col-6 col-sm-6"></div>
        </div>
        <Display/>
      </React.Fragment>
    );
  }
}
export default withRouter(Product);
