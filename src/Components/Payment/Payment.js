import React, { Fragment } from "react";
import StripeCheckout from "react-stripe-checkout";
import axios from "axios";
//import { response } from "express";

const Payment = () => {
  const publishableKey =
    "pk_test_51GsKccH5lFu2WkTkYggYDuXAFoLtM4cHmTxQonBfJsUGcfdPrLio0d4kb8n8ZftbzQqjq5STikcGY7RXslTy2VuW003PscYsNv";

  const onToken = (token) => {
    console.log(token);
    
    const body = {
      amount: 99,
      token: token,
    };

    axios
      .post("/Payment", body)
      .then((response) =>
        console
          .log("Payment Sucess")
          .catch((error) => console.log("error getting"))
      );
  };
  return(
    <StripeCheckout 
    label="Pay"
    name="Bussiness LLC"
    description="Upgrade to a premium account today"
    panelLabel="Premium"
    amount={999}
    token={onToken}
    stripeKey={publishableKey}
    image="https://www.vidhub.co" //Pop-in header image
    billingAddress={false}
    />
  )
};
export default Payment;
