import React, { Component } from "react";
import uuid from "uuid";
import "./add_message.css";
import { withRouter } from "react-router";
import axios from "axios";

class AddMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemName: "",
      itemType: "",
      itemImage: "",
    };
  }

  getProductInformation = (evt) => {
    this.setState({ [evt.target.name]: evt.target.value });
  };

  onFileChange = (event) => {
    // Update the state
    this.setState({ itemImage: event.target.files[0] });
  };

  submitProductInformation = (evt) => {
    const formData = new FormData();
    formData.append("myFile", this.state.itemImage);
    const data = {
      itemId:uuid.v4(),
      itemName: this.state.itemName,
      itemType: this.state.itemType,
      itemImage:
        "https://www.moroccanoil.com/media/catalog/product/h/y/hydrating_shampoo_2.jpg?quality=80&bg-color=255,255,255&fit=bounds&height=550&width=460&canvas=460:550",
    };
    //debugger
    const options = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    axios
      .post("/addProduct", data, options)
      .then((res) => {
        console.log("RESPONSE ==== : ", res);
      })
      .catch((err) => {
        console.log("ERROR: ====", err);
      });
  };

  render() {
    //this.props.history.push('link');
    let { history } = this.props;
    // console.log("add_message",this.state.itemName,this.state.itemType,this.state.itemImage);

    return (
      <React.Fragment>
        <div className="add-message-box">
          <div className="container">
            <form
              action="/addProduct"
              method="post"
              enctype="multipart/form-data"
            >
              <div className="form-group">
                <label for="itemName">Product Name</label>
                <input
                  type="text"
                  className="form-control"
                  id="itemName"
                  placeholder="Product Name"
                  name="itemName"
                  onChange={this.getProductInformation}
                />
              </div>
              <div className="form-group">
                <label for="itemType">Product Type</label>
                <input
                  type="text"
                  className="form-control"
                  id="itemType"
                  placeholder="Product Name"
                  name="itemType"
                  onChange={this.getProductInformation}
                />
              </div>
              <div className="form-group">
                <label for="itemType">Product Image</label>
                <input
                  type="file"
                  className="form-control"
                  id="itemImage"
                  placeholder="Product Image"
                  name="itemImage"
                  multiple
                  accept="image/*"
                  onChange={this.onFileChange}
                />
              </div>
              <div className="submit-message-box">
                <button
                  type="submit"
                  class="btn btn-primary"
                  onClick={this.submitProductInformation}
                >
                  Submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default withRouter(AddMessage);
