export const constantMessage = [
  { name: 'subject', id: 0 },
{ name: 'description', id: 1 }
];

export const textInput = [
  { subject: 'Subject of the message', id: 0, type: 'input' },
{ subject: 'Please write your message', id: 1, type: 'textarea' }
]

export const buttonText = [
  { name: 'New Message', id: 0, type: "primary" },
  { name: 'Clear', id: 1, type: "danger" },
  { name: 'Submit', id: 2, type: "success" }
]
export const buttonPrint = [
  { name: "\u274C", id: 4, buttonType: "rowDelete" },
{ name: 'Update', id: 5, buttonType: "rowUpdate" }
]